const { GoogleSpreadsheet } = require('google-spreadsheet');
const csv = require('csv-parser');
const fs = require('fs');
const Hashset = require('hashset');

//CHANGE VARIABLES HERE
//CONVERT TO GMT, THEN MILLISECONDS
var sheetName = 'ML China';
var startTime = 620454800000;
var endTime = 3620590819000;
var regionTrigger = '#bibi55';
//



const doc = new GoogleSpreadsheet('1VGy6GzfNwJkC8R79OV0i0oyTatJ60SYz7CG3BwSywa0');
var rows = [];
var seenUsers = new Hashset();
var seenTags = new Hashset();

async function getData() {
    fs.createReadStream('giveawayLog.csv')
        .pipe(csv())
        .on('data', async (row) => {
            await processRow(row);
        })
        .on('end', async () => {
            await sendRows();
            await saveNames();
            console.log('CSV file successfully processed');
        });
}

async function processRow(row) {
    let message = row.Message;
    let words = message.split(' ');
    var trigger = words[0].toLowerCase();
    let user = row.User + ',' + row.Platform;
    let tag = words[1];
    if (tag == undefined) return;
    if (tag.charAt(0) == '#') tag = tag.substring(1);

    if (row.Timestamp < startTime || row.Timestamp > endTime) {
        return;
    }
    if (trigger == regionTrigger && tag.match(/^[a-zA-Z0-9]+$/i) && !seenUsers.contains(user) && !seenTags.contains(tag.toLowerCase())) {
        row.Message = tag.replace('O', '0').replace('o', '0').toUpperCase();
        seenUsers.add(user);
        seenTags.add(tag.toLowerCase());
        console.log(tag);
        console.log(user);
        rows.push(row);
    }
}

async function sendRows() {
    console.log('sending Rows');
    console.log(rows);
    await doc.loadInfo();
    var sheet = doc.sheetsByTitle[sheetName];
    await sheet.addRows(rows);
}

async function saveNames() {
    let data = JSON.stringify({
        "names": seenUsers.toArray()
    })
    fs.writeFileSync('seenNames.json', data);
    data = JSON.stringify({
        "tags": seenTags.toArray()
    })
    fs.writeFileSync('seenTags.json', data);
}

async function readNames() {
    fs.readFile('seenNames.json', async (err1, data1) => {
        if (err1) throw err1;
        let seenNames = JSON.parse(data1).names;
        for (const name of seenNames) {
            seenUsers.add(name);
        }
        fs.readFile('seenTags.json', (err, data) => {
            if (err) throw err;
            let tags = JSON.parse(data).tags;
            for (const tag of tags) {
                seenTags.add(tag);
            }
        });
    });

}

//Sheets login
async function sheetsLogin() {
    console.log("Login");
    await doc.useServiceAccountAuth({
        client_email: "reader@giveaway-sheet-reader.iam.gserviceaccount.com",
        private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQChWt/0UUcAZHJr\nI5JTuIb5TJe43Dbp9+fQD2d/1nT13qGxLZiKz7c6cHb0dzC3dYKq1ceYmIhc1RWu\nnjiRnSbYxxayj0/y1I3yOsY8wLSrsaH8Z6Gokxtmc0IlMeQ08aCtgyRmzXKjLOt9\nHaj3x6DBQpd+D7slpC2YYy5NK0ZxyHaXM8vAhCjPNFmC4SLoBTg4LuK0ir+2ufYe\nNFG7I/TKrxWGi92xMbLsqY3J29vm5JNtJ0Xw8KJkEHwTDaD+Q1ILUSPltOMtk/C4\n1GvCdgY1xyCABP5FzDNCmSKJ27hmuzYdYAHDknJj8otb7a8zdrCHX16hd125T2Xk\n4Y3H7mKBAgMBAAECggEACuAeaLACvlzG1oBVCEYPeOWn80qKe/4c1on3QqdQhlLt\naI0Hc1G5SpvSdLtE4hSuTDfxBlC/niwxlC2vZ+ck+2b92Wat75LSh3FdM7KUat8j\n6SiN/Zf+uIrLukiQ1l2z5AYLEc/GqodoTuxNG3hN1kDYB6PlTA3zEcqbkBF648fR\nCnc5E9x5ZQADqqz1CHXjYCbwTGdHxNjVLSold3W1EO9JUSkwKf1L09+B/rFQMiju\nSwPFxSMUH2sqA9bzrHTCMD8e6NWtaIAWlL/S3Ry73u+GdCj47Rkqi8BsQ6NE/C5m\nZGHEsB9FcmJsNlHFqdTpUiNYr0WvcOea7r36BNQpMQKBgQDMvHSjFKYTlnWTQYpn\nGYsn7Qp2JDODaY2se57BXYP1aer8RdDZyqouwPBK5tnNMvC8NZkts7NmqvD6f/Hl\nWQHi0cAq8cOaPUwldjJpQMZPVJo1chkyGm85sYSq1QO8zYEsWsF8hFWWnA0cl9D0\nF3pA1PJfL5WjhqUb6DuthVIliQKBgQDJwbHtoA5m99dhiJnvM9vFJy1/MN7W355P\n3YjKtJNnyhtoO8ojFZOJuYEypdkavpvalN9nKgWCZiFDXDLBMzRxuyny+L9qVtEc\n2vUjnetK7ecLuuQR8VD4S+YAZ0FHen0LPHlWrB9P6bD6aHylkrsRtMrOwY/4Ue3U\nficdOakPOQKBgD0iFK3Rb5mvtqHm0x3f+bZNSG9odd1KelxDeW8BrnJsUxdK/Yba\nxiA5ykaDUgu8pzd7xaZo3yvL5TLXW3KBNlf1C62HDUIUBZW2Cm11QwHTIEzPKgjU\n7dtLT3L3MiuBLY4B935nwWq8ZAnipykLjZyoOUsE2Wi/vrNrj85LXfEJAoGARJrN\nsvzDCPZr0amOlo0uxPqOePqYO76saYHuOuduXFOYQDhcgtOaaLGgoiVNDtAT1R7c\nXIsodm/eDbazN4j+41F+cTVueW1uC/7UbrkLQl5lO1LgrM0++D9uxWJXC0Q2cvSs\nWfGLcDQJzwxqni6n19x/PhEJsKnDUodXK5PozdkCgYEAzHqBXaOKdPJrxhQSk226\nfjyosTdsanLG+iDxOC2pFyig6U0pd4eNEnKAVoBT0rlhV4xgv0M9tEubsBaLuhL8\newL7AaH4c/kPkGlCYjuvBdogjZtUkD7nUo6XWPCKB1FwLgKV1yF5gFRkLgwcEw72\n8b0G4MMQvGkZdaaOkWk6uxk=\n-----END PRIVATE KEY-----\n",
    });
    console.log('Done Login');
    return;
}



async function main() {
    await sheetsLogin();
    await readNames();
    setTimeout(function () { getData(); }, 2000)
}

main();